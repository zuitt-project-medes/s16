// alert("hi")

let number = Number(prompt("Please provide a number:"));
console.log("The number you provided is " + number)

for(i=number; i>=0; i--) {

	if(i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if(i % 5 === 0) {
		console.log(i);
		continue;
	}

	if(i <= 50) {
		console.log("The current value is at 50. Terminating the loop")
		break;
	}
}
