// alert("hello")

/*
Mini-Activity - 5 mins

Create a function called  displayMsgToSelf()

-display message to your past self in your console

Invoke the function 10 times

*/

// function displayMsgToSelf(){
//  console.log("Hi Kim!")
// };

// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();

// let count = 10;

// while(count !== 0){
// 	displayMsgToSelf();
// 	count--;
// }


// While loop will allow us to repeat an instructions as long as the condition
// is true.

/*
Syntax:

while(condition is true){
	statement/ instruction runs incrementation or decrementation to stop the loop
}
*/

let number = 5;
while(number !== 0){
	// the curent value will be printed out
	console.log("While" + " " + number);
	// decreases the count by  1 after every iteration to stop the loop when it reaches zero
	number--;
};

// count from 5-10

while(number !== 10){
	console.log("While" + " " + number);
	number++;
};

// do-while loop
// A do-while loop works a lot like a while loop but unlike while loop, do-while guarantees to run at least once
/*
Syntax:

do{
	statement
} while(condition)
*/

// Number function works like parseInt, it chang a string type to number data type
let number2 = Number(prompt("Give me a number"))

do{
	// the current value of the number2 is printed out
	console.log(`Do While ${number2}`);

	// increases the number by 1 after every iteration of the loop
	number2 += 1;

	// while(condition)
} while(number2 < 10);

let counter = 1;

do{
	console.log(counter);
	counter++
} while(counter <= 21);


// For Loop

// a more flexible loop than while and do-while loop.
// it consists of three parts:
// initialization, condition and finalExpression
 //initialization- determines where the loop starts
 //condition- determines when the loop stops
 //finalExpression- indicates how to the advance loop (incrementation or decrementation)

 /*
Syntax:

for(initialization; condition;
finalExpression){
	statement
}


 */ 

for (let count = 0; count <= 20; count++){
	console.log(count);
};

let myString = "alex"
console.log(myString.length)
// result: 4
// note: strings are special compared to other data types
// it has access to functions and other pieces of information another peimitive might not have

// myString = ["a", "l", "e", "x"]

console.log(myString[0]);
// result: a
console.log(myString[3]);
// result: x

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "KimJoseph"

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){ console.log(3)
} else {
	console.log(myName[i])
}

};

// Continue and Break Statements

for(let count = 0; count <= 20; count++){
	if (count % 2 === 0) {
		continue;
	}
	console.log(`Continue and Break ${count}`)

	if(count > 10){
		break;
	}
}

let name = "alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration")
		continue;
	}
	if(name[i] == "d"){
		break;
	} 
}

























































